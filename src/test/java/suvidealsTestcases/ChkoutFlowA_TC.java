package suvidealsTestcases;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.suvideals.qa.base.TestBaseSuvideals;
import com.suvideals.qa.pages.ChkoutFlowA;
import com.suvideals.qa.pages.HomePageSuvideals;
import com.suvideals.qa.pages.LoginRegisterPageSuvideals;

public class ChkoutFlowA_TC extends TestBaseSuvideals
{
	@BeforeTest
	public static void openURL() throws IOException
	{
		initialization();
	}
	
	@BeforeMethod
	public static void homePage()
	{
		HomePageSuvideals HP= new HomePageSuvideals(driver);
		HP.clickLoginSignUp();
		
		String exEmail="abc2590@gmail.com";
		String exPass="Nature@1";
		
		LoginRegisterPageSuvideals homePageObj=new LoginRegisterPageSuvideals(driver);
		homePageObj.verfiyLogin(exEmail, exPass);
	}
	
	@Test
	public void buyFirstProdTC() throws InterruptedException
	{
		ChkoutFlowA buyFP=new ChkoutFlowA(driver);
		buyFP.buyFirstProd();
	}
}