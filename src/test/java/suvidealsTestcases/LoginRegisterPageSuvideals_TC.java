package suvidealsTestcases;

import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.suvideals.qa.base.TestBaseSuvideals;
import com.suvideals.qa.pages.HomePageSuvideals;
import com.suvideals.qa.pages.LoginPageSuvideals;
import com.suvideals.qa.pages.LoginRegisterPageSuvideals;

public class LoginRegisterPageSuvideals_TC extends TestBaseSuvideals
{
	@BeforeTest
	public static void openURL() throws IOException
	{
		initialization();
	}
	
	@BeforeMethod
	public static void loginRegPage()
	{
		HomePageSuvideals homePage=new HomePageSuvideals(driver);
		homePage.clickLoginSignUp();		
	}
	
	@Test (priority=0)
	public static void createNewAct() throws InterruptedException
	{	
		String nwCustNm, nwCustEml, nwCustPass;
		nwCustNm="abc";
		nwCustEml="abc2590@gmail.com";
		nwCustPass="Nature@1";
		LoginRegisterPageSuvideals loginRegPage=new LoginRegisterPageSuvideals(driver);
		loginRegPage.createNewAct(nwCustNm,nwCustEml,nwCustPass);
	}
	
	@Test (priority=1)
	public static void verifyLogin()
	{	
		String exstEmail,exstPass;
		exstEmail="abc2590@gmail.com";
		exstPass="Nature@1";
		
		LoginRegisterPageSuvideals loginRegPage= new LoginRegisterPageSuvideals(driver);
		loginRegPage.verfiyLogin(exstEmail, exstPass);
		
		LoginPageSuvideals LP=new LoginPageSuvideals(driver);
		String actualuName=LP.getUserName();
		String emailN=exstEmail.substring(0, 3).toUpperCase();
		String expecteduName="HELLO"+" "+emailN;
		System.out.println("Actual user name is " + actualuName+ " "+expecteduName);
		if (actualuName.equals(expecteduName))
			Reporter.log("Successfully logged in ....");
	}
	@AfterTest
	public static void closeURL()
	{
		quitSession();
	}

}