package suvidealsTestcases;

import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.suvideals.qa.base.TestBaseSuvideals;
import com.suvideals.qa.pages.HomePageSuvideals;

public class HomePageSuvideals_TC extends TestBaseSuvideals
{
	@BeforeMethod
	public static void openURL() throws IOException
	{
		initialization();
	}
	
	@Test (priority=0)
	public static void verifyHomePageTitle()
	{
		String expectedHPTitle="Deal of the Day, Best Online Shopping Deals Today, Daily Deals on Magic Box - Suvideals.ooo";	
		String actualHPTitle=driver.getTitle();
	
		Assert.assertEquals(actualHPTitle, expectedHPTitle, "Title is not matched");
	}
	
	@Test (priority=1)
	public static void verifyWareHousePage()
	{
		HomePageSuvideals homePage=new HomePageSuvideals(driver);
		homePage.getWarehouseDealsPage();
	}
	
	@AfterMethod
	public static void closeURL()
	{
		quitSession();
	}
}