package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageSuvideals
{
	WebDriver driver;
	
	@FindBy(xpath="/html/body/div[1]/div/div[2]/div[2]/a[1]")
	WebElement userName;
	
	public LoginPageSuvideals(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	public String getUserName()
	{
		return userName.getText();
	}
}