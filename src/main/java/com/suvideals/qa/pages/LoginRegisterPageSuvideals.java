package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.suvideals.qa.base.TestBaseSuvideals;

public class LoginRegisterPageSuvideals extends TestBaseSuvideals
{
	WebDriver driver;
	
	@FindBy(id="new-account-btn")
	WebElement newAct;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div[1]/div/div[2]/div/div/div/form/div[1]/div/input")
	WebElement newCustName;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div[1]/div/div[2]/div/div/div/form/div[2]/div/input")
	WebElement newCustEmail;
	
	@FindBy(xpath = "/html/body/div[2]/div/div/div[1]/div/div[2]/div/div/div/form/div[3]/div/input")
	WebElement newCustPass;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div[1]/div/div[2]/div/div/div/form/div[5]/div/button")
	WebElement crtNwCust;
	
	@FindBy(xpath="//*[@id=\"userHandle\"]")
	WebElement exstEmail;
	
	@FindBy(xpath="//*[@id=\"password\"]")
	WebElement exstPass;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div[1]/div/div[1]/div[1]/div/form/div[6]/div/button")
	WebElement login;

	public LoginRegisterPageSuvideals(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	public void createNewAct(String nwCustName, String nwCustEmail, String nwCustPass) throws InterruptedException
	{
		newAct.click();
		newCustName.sendKeys(nwCustName);
		newCustEmail.sendKeys(nwCustEmail);
		newCustPass.sendKeys(nwCustPass);
		crtNwCust.click();
	}
	
	public LoginPageSuvideals verfiyLogin(String exEmail,String exPass)
	{
		exstEmail.sendKeys(exEmail);
		exstPass.sendKeys(exPass);
		login.click();
		return new LoginPageSuvideals(driver);
	}
}