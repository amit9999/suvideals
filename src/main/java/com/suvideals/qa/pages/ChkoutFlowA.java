package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ChkoutFlowA
{
	WebDriver driver;
	
	public ChkoutFlowA(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	@FindBy(xpath="/html/body/div[1]/div/div[3]/div[5]/a")
	WebElement shopCart;
	
	public void buyFirstProd() throws InterruptedException
	{
		shopCart.click();
		
		ShopCartPageA cartPg=new ShopCartPageA(driver);
		cartPg.addToShopCart();
		
		ShopCartPageB cartPgB=new ShopCartPageB(driver);
		cartPgB.confirmAddress();
		
		ShopCartPageC cartPgC=new ShopCartPageC(driver);
		cartPgC.addPayment();
		cartPgC.selectPayment();
	}
}