package com.suvideals.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ShopCartPageC
{
	WebDriver driver;

	@FindBy(xpath="/html/body/div[1]/div[1]/div/div[2]/div/div/div/accordion/div/div[2]/div[2]/div/div/div/div[5]/div[1]/div[1]/button")
	WebElement addPaymnt;
	
	@FindBy(xpath="/html/body/div[1]/div[1]/div/div[2]/div/div/div/accordion/div/div[4]/div[2]/div/div/div/div/div[1]/div/div/div/div[1]/ul/li[3]/a")
	WebElement debitCard;
	
	@FindBy (xpath="/html/body/div[1]/div[1]/div/div[2]/div/div/div/accordion/div/div[4]/div[2]/div/div/div/div/div[1]/div/div/div/div[2]/div/div[3]/custom-tab-content/div/div[1]/div/div/div[1]/div[4]/iframe")
	WebElement frame;
	
	@FindBy(xpath="//*[@id=\"debitCard\"]")
	WebElement debitType;
	
	@FindBy(xpath="//*[@id=\"debitCardNumber\"]")
	WebElement cardNo;
	
	@FindBy(xpath="//*[@id=\"expiryMonthDebitCard\"]")
	WebElement month;
	
	@FindBy(xpath="//*[@id=\"expiryYearDebitCard\"]")
	WebElement year;
	
	@FindBy(xpath="//*[@id=\"SubmitBillShip\"]")
	WebElement makePymnt;
	
	public ShopCartPageC(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	public void addPayment()
	{
		addPaymnt.click();
	}
	
	public void selectPayment() throws InterruptedException
	{
		debitCard.click();

		WebElement ele=driver.findElement(By.tagName("iframe"));
		System.out.println(ele.getAttribute("class"));
		if (ele.getText()=="iframe")
		{
			driver.switchTo().frame(ele);
			Select slc=new Select(debitType);
			slc.selectByVisibleText("MasterCard Debit Card");
			
			cardNo.clear();
			cardNo.sendKeys("5425233430109903");
			
			Select slc1=new Select(month);
			slc1.selectByIndex(4);
			
			Select slc2=new Select(year);
			slc2.selectByVisibleText("2023");
			
			//makePymnt.click();
		}
		else
			System.out.println("Frame is currntly not interactable");
	}
}