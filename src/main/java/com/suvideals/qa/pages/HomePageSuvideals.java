package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.suvideals.qa.base.TestBaseSuvideals;

public class HomePageSuvideals extends TestBaseSuvideals
{
	WebDriver driver;

	@FindBy(xpath="/html/body/div[2]/div/div/div[1]/div/div[1]/div[4]/a/div[1]/img")
	WebElement weeklyDeal;
	
	@FindBy(xpath="/html/body/div[1]/div/div[2]/div[2]/a[1]")
	WebElement loginSignUp;
	
	public HomePageSuvideals(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}

	public  WarehouseDeals getWarehouseDealsPage()
	{
		weeklyDeal.click();
		return new WarehouseDeals(driver);
	}
	
	public void clickLoginSignUp()
	{
		loginSignUp.click();
	}
}