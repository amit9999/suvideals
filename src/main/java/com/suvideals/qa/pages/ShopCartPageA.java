package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShopCartPageA
{
	WebDriver driver;

	@FindBy(xpath="/html/body/div[2]/div/div/div/div[3]/form/div/div[2]/div[1]/button")
	WebElement addToCart;
	
	public ShopCartPageA(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	public void addToShopCart()
	{
		addToCart.click();
	}
}