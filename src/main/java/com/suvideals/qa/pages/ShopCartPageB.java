package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShopCartPageB
{
	WebDriver driver;

	@FindBy(xpath="/html/body/div[1]/div[1]/div/div[2]/div/div/div/accordion/div/div[1]/div[2]/div/div/div/form/div[2]/div[5]/div[1]/button")
	WebElement confirmAdd;
	
	public ShopCartPageB(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	public void confirmAddress()
	{
		confirmAdd.click();
	}	
}