package com.suvideals.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.suvideals.qa.base.TestBaseSuvideals;

public class WarehouseDeals extends TestBaseSuvideals
{
	WebDriver driver;
	
	@FindBy(xpath="/html/body/div[2]/div/div/div/div[2]/div/div/div/div[1]/div/h3")
	WebElement pageHeading;
	
	public WarehouseDeals(WebDriver driver)
	{
		this.driver = driver; 
		PageFactory.initElements(driver, this);		
	}
	
	public String getWHHeading()
	{
		return pageHeading.getText();
	}
}