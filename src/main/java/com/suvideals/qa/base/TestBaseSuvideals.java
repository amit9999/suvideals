package com.suvideals.qa.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestBaseSuvideals
{
	protected static WebDriver driver;
	
	public static void initialization() throws IOException
	{
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver=new ChromeDriver();
		
		Properties prop =new Properties();
		FileInputStream objFile=new FileInputStream(System.getProperty("user.dir")+"\\src/main/java\\com/suvideals/qa/config\\suvideals.properties");
		prop.load(objFile);
		driver.get(prop.getProperty("URL"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	public static void quitSession()
	{
		driver.quit();
	}
}